plugins {
    `java-library`
    id("application")
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

val groupId: String by project
val vcpxGroupId: String by project
val edcVersion: String by project

dependencies {
    api("${groupId}:boot:${edcVersion}")

    implementation("${groupId}:control-plane-core:${edcVersion}")
    implementation("${groupId}:api-observability:${edcVersion}")

    // IDS stuff such as IDS messages
    implementation("${groupId}:ids:${edcVersion}") {
        exclude("${groupId}", "ids-token-validation")
    }

    //Updated milestone-7 to milestone-8
    //implementation("${groupId}:data-management-api:${edcVersion}")
    implementation("${groupId}:management-api:${edcVersion}")

    // Data transfer (read from AAS service/write to HTTP endpoint)
    implementation("${groupId}:data-plane-core:${edcVersion}")
    implementation("${groupId}:data-plane-http:${edcVersion}")
    //Updated milestone-7 to milestone-8
    //implementation("${groupId}:data-plane-transfer-client:${edcVersion}")
    implementation("$groupId:data-plane-selector-client:$edcVersion")
    implementation("$groupId:data-plane-selector-core:$edcVersion")
    implementation("${groupId}:transfer-data-plane:${edcVersion}")

    // Read configuration values
    implementation("${groupId}:configuration-filesystem:${edcVersion}")

    // Identity and access management MOCK -> only for testing
    implementation("${groupId}:iam-mock:${edcVersion}")
    implementation("${groupId}:auth-tokenbased:${edcVersion}")

    // fie to http transfer extension
    implementation("${vcpxGroupId}:file-to-http-extension:${edcVersion}")

    // Endpoint
    implementation(libs.jakarta.rsApi)
}

application {
    mainClass.set("com.qsurf.vcpx.app.runtime.CustomRuntime")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    isZip64 = true
    exclude("**/pom.properties", "**/pom.xm")
    mergeServiceFiles()
    archiveFileName.set("vcp-x-connector.jar")
}