/*
 *  Copyright (c) 2022 QuantumSurf Co., Ltd.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

package com.qsurf.vcpx.app.web;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Delegates (HTTP) Requests to controllers.
 */
@Consumes({ MediaType.APPLICATION_JSON, MediaType.WILDCARD })
@Produces({ MediaType.APPLICATION_JSON })
@Path("/")
public class Endpoint {
    private final Logger logger;

    public Endpoint() {
        this.logger = Logger.getInstance();
    }

    @POST
    @Path("receiveTransfer/model/{param}")
    public void modelReceive(@PathParam("param") String param, String body) {
        // Testing a data transfer to an HTTP endpoint
        logger.log(body);
    }

    @POST
    @Path("receiveTransfer/data/{param}")
    public void dataReceive(@PathParam("param") String param, String body) {
        // Testing a data transfer to an HTTP endpoint
        logger.log(body);
    }
}
