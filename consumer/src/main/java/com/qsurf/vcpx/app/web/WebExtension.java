/*
 *  Copyright (c) 2022 QuantumSurf Co., Ltd.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

package com.qsurf.vcpx.app.web;

import org.eclipse.edc.api.auth.spi.AuthenticationService;
import org.eclipse.edc.runtime.metamodel.annotation.Extension;
import org.eclipse.edc.runtime.metamodel.annotation.Inject;
import org.eclipse.edc.spi.system.ServiceExtension;
import org.eclipse.edc.spi.system.ServiceExtensionContext;
import org.eclipse.edc.web.spi.WebService;

/**
 * Extension for receiving data from the provider.
 */
@Extension(value = WebExtension.NAME)
public class WebExtension implements ServiceExtension {
    public static final String NAME = "web server for receiving data from provider";

    private final Logger logger;

    @Inject
    private WebService webService;

    @Inject
    private AuthenticationService authenticationService;

    private Endpoint endpoint;

    public WebExtension() {
        logger = Logger.getInstance();
    }

    /**
     * get extension name
     *
     * @return String
     */
    @Override
    public String name() {
        return NAME;
    }

    /**
     * initialize service extension
     *
     * @param context service extension context
     */
    @Override
    public void initialize(ServiceExtensionContext context) {
        logger.setMonitor(context.getMonitor());

        endpoint = new Endpoint();

        webService.registerResource(endpoint);
        webService.registerResource(new CustomAuthenticationRequestFilter(authenticationService));
    }

    /**
     * shutdown service extension
     */
    @Override
    public void shutdown() { }
}
