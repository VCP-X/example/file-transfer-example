/*
 *  Copyright (c) 2022 QuantumSurf Co., Ltd.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

package com.qsurf.vcpx.app.web;

import jakarta.ws.rs.container.ContainerRequestContext;
import org.eclipse.edc.api.auth.spi.AuthenticationRequestFilter;
import org.eclipse.edc.api.auth.spi.AuthenticationService;

import java.util.Objects;

/**
 * Custom AuthenticationRequestFilter filtering requests.
 */
public class CustomAuthenticationRequestFilter extends AuthenticationRequestFilter {
    public CustomAuthenticationRequestFilter(AuthenticationService authenticationService) {
        super(authenticationService);
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        Objects.requireNonNull(requestContext);

        // super.filter(requestContext);
    }
}
