/*
 *  Copyright (c) 2022 QuantumSurf Co., Ltd.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

package com.qsurf.vcpx.app.runtime;

import org.eclipse.edc.boot.system.DefaultServiceExtensionContext;
import org.eclipse.edc.boot.system.runtime.BaseRuntime;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.system.ConfigurationExtension;
import org.eclipse.edc.spi.system.ServiceExtensionContext;
import org.eclipse.edc.spi.telemetry.Telemetry;
import org.eclipse.edc.spi.types.TypeManager;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CustomRuntime extends BaseRuntime {
    private static final String RUNTIME_NAME = "VCP-X connector";

    public static void main(String[] args) {
        new CustomRuntime().boot();
    }

    @Override
    protected String getRuntimeName(ServiceExtensionContext context) {
        return RUNTIME_NAME;
    }

    @Override
    protected @NotNull ServiceExtensionContext createContext(TypeManager typeManager, Monitor monitor, Telemetry telemetry) {
        return new SuperCustomExtensionContext(typeManager, monitor, telemetry, loadConfigurationExtensions());
    }

    @Override
    protected void shutdown() {
        super.shutdown();

        monitor.info(String.format("%s shutdown!", RUNTIME_NAME));
    }

    private static class SuperCustomExtensionContext extends DefaultServiceExtensionContext {
        SuperCustomExtensionContext(TypeManager typeManager, Monitor monitor, Telemetry telemetry, List<ConfigurationExtension> configurationExtensions) {
            super(typeManager, monitor, telemetry, configurationExtensions);
        }
    }
}
