/*
 *  Copyright (c) 2022 QuantumSurf Co., Ltd.
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       QuantumSurf Co., Ltd. - initial API and implementation
 *
 */

package com.qsurf.vcpx.app.web;

import org.eclipse.edc.spi.monitor.Monitor;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * logging class using org.eclipse.edc.spi.monitor.Monitor.
 */
public class Logger {
    private static final String PREFIX_SEPARATOR = ": ";
    private static Logger instance;

    private String prefix = "ReceiveData";
    private Monitor monitor;

    /**
     * Get the instance of this singleton.
     * If no instance is available, one will be created.
     *
     * @return Instance of this class.
     */
    public static synchronized Logger getInstance() {
        if (Objects.isNull(instance)) {
            instance = new Logger();
        }

        return instance;
    }

    /**
     * Set a new prefix for this logger.
     *
     * @param prefix The new prefix
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * Get the prefix of this logger.
     *
     * @return The prefix of this logger
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Only to be called by the extension itself.
     *
     * @param monitor The monitor used to log stuff from
     */
    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    /**
     * Log a message with severity "info"
     *
     * @param message Message to be logged
     */
    public void log(String message) {
        monitor.info(prefix + PREFIX_SEPARATOR + message);
    }

    /**
     * Log a message with severity "info"
     *
     * @param message Message to be logged
     */
    public void log(String... message) {
        monitor.info(prefix + PREFIX_SEPARATOR + Stream.of(message).collect(Collectors.joining(" ")));
    }

    /**
     * Log a message with severity "debug"
     *
     * @param message Message to be logged
     */
    public void debug(String message) {
        monitor.debug(prefix + PREFIX_SEPARATOR + message);
    }

    /**
     * Log a message with severity "warn"
     *
     * @param message Message to be logged
     * @param errors errors
     */
    public void warn(String message, Throwable... errors) {
        monitor.warning(prefix + PREFIX_SEPARATOR + message, errors);
    }

    /**
     * Log a message with severity "error"
     *
     * @param message Message to be logged
     * @param errors errors
     */
    public void error(String message, Throwable... errors) {
        monitor.severe(prefix + PREFIX_SEPARATOR + message, errors);
    }
}
