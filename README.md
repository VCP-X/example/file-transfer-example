# File Transfer Example

## 개요

EDC를 활용하여 로컬 또는 클라우드 저장소에 있는 파일을 http를 통해 전달하는 예제입니다.


## 개발 환경

### 개발 툴
1. IDE : IntelliJ IDEA Ultimate 21.2.4

2. JDK : Version 16

### 개발 환경 구축
IDE의 경우 [JetBrains 홈페이지](https://www.jetbrains.com/ko-kr/idea/download/other.html)에서 상기 버전을 확인하여 설치합니다. JDK의 경우 IntelliJ IDEA에서 제공하는 JDK version 16을 권장합니다.

<<<<<<< HEAD
## 빌드

1. IntelliJ IDEA Ultimate 21.2.4를 실행하여 해당 프로젝트를 불러옵니다.

2. 하단 부 탭에서 **Terminal(기본 단축키 : Alt+F12)** 을 실행합니다.

3. Gradle Wrapper를 사용하여 빌드를 하기 위한 명령어를 터미널에 입력합니다.
    ```
    .\gradlew build
    ```

    * 코드 수정 시 반영이 되지 않고 빌드가 된다면 아래의 명령어를 통해 빌드를 합니다.
        ```
        .\gradlew clean build
        ```

## 설정
해당 예제는 프로젝트 하나에 Provider, Consumer가 존재합니다. 해당 모듈에 필요한 설정 사용자 환경에 맞게 수정하여 설정합니다.

### Provider
1. provider 내에 있는 **configurations** 폴더 내 **provider.properties** 를 엽니다.
    * 해당 파일은 Provider EDC에서 사용하는 설정 값을 정의한 파일입니다.

2. 전송할 파일의 경로를 사용자 환경에 맞게 수정합니다.
    ```
    # 절대 경로로 작성할 것
    edc.asset.path=<경로/파일명.확장자>
    ```

3. Provider의 IP를 사용자 환경에 맞게 수정합니다.
    ```
    # 기본 설정값은 localhost로 되어 있으며 필요 시 provider가 실행되는 host의 IP를 입력
    ids.webhook.address=http://<IP>:8282
    ```

### Consumer
1. consumer 내에 있는 **configurations** 폴더 내 **consumer.properties** 를 엽니다.
    * 해당 파일은 Consumer EDC에서 사용하는 설정 값을 정의한 파일입니다.

2. Consumer의 IP를 사용자 환경에 맞게 수정합니다.
    ```
    # 기본 설정값은 localhost로 되어 있으며 필요 시 consumer가 실행되는 host의 IP를 입력
    ids.webhook.address=http://<IP>:9292
    ```

## 실행
* 해당 설명에서는 [Postman](https://www.postman.com/)을 활용합니다.
* 설명하는 순서와 다르게 진행 시 에러가 발생할 수 있습니다.
1. **Provider** 와 **Consumer** 를 실행 시키기 위해 **Terminal** 에 아래의 명령어를 실행합니다.
    ```
    # Provider 실행
    java -Dedc.fs.config=provider/configurations/provider.properties -jar provider/build/libs/vcp-x-connector.jar
    ```

    ```
    # Consumer 실행
    java -Dedc.fs.config=consumer/configurations/consumer.properties -jar consumer/build/libs/vcp-x-connector.jar
    ```

2. Postman을 실행합니다.

3. Asset을 설정합니다.
    * [설정->Provider](###Provider)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Provider IP\>:8182/api/v1/management/assets
    * HTTP METHOD = POST
    * Header 추가 목록
        * x-api-key = password
        * Content-Type = application/json
    * Body에 **"REST_API/asset_post.json"** 내용을 입력
        ```json
        {
            "asset": {
                "properties": {
                    "asset:prop:id": "test-document"
                }
            },
            "dataAddress":{
                "properties":{
                    "type":"File",
                    "path":"/home/vagrant/",
                    "filename":"postman.json"
                }
            }
        }
        ```
        * dataAddress에 있는 "path", "filename"은 **provider/configurations/provider.properties"** 에서 수정한 **edc.asset.path** 값
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        {
            "createdAt": 1675145918950,
            "id": "test-document"
        }
        ```
        * "createdAt"은 통신 시점에 따라 달라질 수 있습니다.

4. Asset을 검색합니다. (저장된 Asset 전체 검색)
    * [설정->Provider](###Provider)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Provider IP\>:8182/api/v1/management/assets
    * HTTP METHOD = GET
    * Header 추가 목록
        * x-api-key = password
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        [
            {
                "createdAt": 1675145918950,
                "properties": {
                    "asset:prop:id": "test-document"
                },
                "id": "test-document"
            }
        ]
        ```

5. Policy를 설정합니다.
    * [설정->Provider](###Provider)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Provider IP\>:8182/api/v1/management/policydefinitions
    * HTTP METHOD = POST
    * Header 추가 목록
        * x-api-key = password
        * Content-Type = application/json
    * Body에 **"REST_API/policy_post.json"** 내용을 입력
        ```json
        {
            "policy": {
                "uid": "956e172f-2de1-4501-8881-057a57fd0e69",
                "permissions": [
                    {
                        "edctype": "dataspaceconnector:permission",
                        "uid": null,
                        "target": "test-document",
                        "action": {
                            "type": "USE",
                            "includedIn": null,
                            "constraint": null
                        },
                        "assignee": null,
                        "assigner": null,
                        "constraints": [],
                        "duties": []
                    }
                ],
                "prohibitions": [],
                "obligations": [],
                "extensibleProperties": {},
                "inheritsFrom": null,
                "assigner": null,
                "assignee": null,
                "target": null,
                "@type": {
                    "@policytype": "set"
                }
            }
        }
        ```
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        {
            "createdAt": 1675146011893,
            "id": "34ac6eae-ce3d-44c5-bb94-e625dab2285a"
        }
        ```
        * "createdAt" 및 "id"는 통신 시점에 따라 달라질 수 있습니다.

6. Policy를 검색합니다. (저장된 Asset 전체 검색)
    * [설정->Provider](###Provider)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Provider IP\>:8182/api/v1/management/policydefinitions
    * HTTP METHOD = GET
    * Header 추가 목록
        * x-api-key = password
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        [
            {
               "createdAt": 1675146011893,
                "id": "34ac6eae-ce3d-44c5-bb94-e625dab2285a",
                "policy": {
                            "permissions": [
                                {
                                    "edctype": "dataspaceconnector:permission",
                                    "uid": null,
                                    "target": "test-document",
                                    "action": {
                                                "type": "USE",
                                                "includedIn": null,
                                                 "constraint": null
                                    },
                                    "assignee": null,
                                    "assigner": null,
                                    "constraints": [],
                                    "duties": []
                                }
                            ],
                            "prohibitions": [],
                            "obligations": [],
                            "extensibleProperties": {},
                            "inheritsFrom": null,
                            "assigner": null,
                            "assignee": null,
                            "target": null,
                            "@type": {
                                "@policytype": "set"
                            }
                    }
            }
        ]
        ```

7. Policy를 생성하여 할당받은 id를 복사한다.
    ```
    # "id"에 있는 값
    34ac6eae-ce3d-44c5-bb94-e625dab2285a
    ```

8. Contract를 설정합니다.
    * [설정->Provider](###Provider)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Provider IP\>:8182/api/v1/management/contractdefinitions
    * HTTP METHOD = POST
    * Header 추가 목록
        * x-api-key = password
        * Content-Type = application/json
    * Body에 **"REST_API/contract_post.json"** 내용을 입력
        ```json
        {
            "id":"1",
            "accessPolicyId":"34ac6eae-ce3d-44c5-bb94-e625dab2285a",
            "contractPolicyId":"34ac6eae-ce3d-44c5-bb94-e625dab2285a",
            "criteria": [
                {
                    "operandLeft": "asset:prop:id",
                    "operandRight": "test-document",
                    "operator": "="
                }
            ]
        }
        ```
        * "accessPolicyId"와 "contractPolicyId"의 값을 이전 Policy에서 생성하여 할당받아 복사해두었던 id를 입력합니다.
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        {
            "createdAt": 1675146053126,
            "id": "1"
        }
        ```
        * "createdAt" 는 통신 시점에 따라 달라질 수 있습니다.

9. Contract를 검색합니다. (저장된 Asset 전체 검색)
    * [설정->Provider](###Provider)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Provider IP\>:8182/api/v1//management/contractdefinitions
    * HTTP METHOD = GET
    * Header 추가 목록
        * x-api-key = password
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        [
             {
                "createdAt": 1675146053126,
                "id": "1",
                "accessPolicyId": "34ac6eae-ce3d-44c5-bb94-e625dab2285a",
                "contractPolicyId": "34ac6eae-ce3d-44c5-bb94-e625dab2285a",
                "criteria": [
                    {
                        "operandLeft": "asset:prop:id",
                        "operator": "=",
                        "operandRight": "test-document"
                    }
                ]
            }
        ]
        ```

10. Contract Negotiation을 진행합니다.
    * [설정->Consumer](###Consumer)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Consumer IP\>:9192/api/v1/management/contractnegotiations
    * HTTP METHOD = POST
    * Header 추가 목록
        * x-api-key = password
        * Content-Type = application/json
    * Body에 **"REST_API/contractnegotiation_post.json"** 내용을 입력
        ```json
        {
            "connectorId": "provider",
            "connectorAddress": "http://\<Provider IP\>:8282/api/v1/ids/data",
            "protocol": "ids-multipart",
            "offer": {
                "offerId": "1:3a75736e-001d-4364-8bd4-9888490edb58",
                "assetId": "test-document",
                "policy": {
                "uid": "34ac6eae-ce3d-44c5-bb94-e625dab2285a",
                "permissions": [
                    {
                        "edctype": "dataspaceconnector:permission",
                        "uid": null,
                        "target": "test-document",
                        "action": {
                                    "type": "USE",
                                    "includedIn": null,
                                    "constraint": null
                        },
                    "assignee": null,
                    "assigner": null,
                    "constraints": [],
                    "duties": []
                        }
                    ],
                    "prohibitions": [],
                    "obligations": [],
                    "extensibleProperties": {},
                    "inheritsFrom": null,
                    "assigner": null,
                    "assignee": null,
                    "target": null,
                    "@type": {
                            "@policytype": "set"
                    }
                },
                "asset": {
                      "properties": {
                            "ids:byteSize": null,
                            "asset:prop:id": "test-document",
                            "ids:fileName": null
                        }
                }
            }
        }
        ```
        * "uid"의 값을 이전 Policy에서 생성하여 할당받아 복사해두었던 id를 입력합니다.
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        {
             "createdAt": 1675146086492,
              "id": "034697e7-574a-4a74-81da-e0107fed9072"
        }
        ```
        * "createdAt" 및 "id" 는 통신 시점에 따라 달라질 수 있습니다.
    * Response의 "id"를 복사합니다.

11. Contract Negotiation 결과를 검색합니다.
    * [설정->Consumer](###Consumer)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Consumer IP\>:9192/api/v1/management/contractnegotiations/\<Contract Negotiation Post 후 얻은 id 값\>
    * HTTP METHOD = GET
    * Header 추가 목록
        * x-api-key = password

    * URI 에 Contract Negotiation Post 시 얻은 "id" 값을 추가합니다.
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        {
            "createdAt": 1675146086492,
            "updatedAt": 1675146091584,
            "contractAgreementId": "1:27c2c1a6-6895-474b-a7a9-0dbe52801aa6",
            "counterPartyAddress": "http://\<Provider IP\>:8282/api/v1/ids/data",
            "errorDetail": null,
            "id": "034697e7-574a-4a74-81da-e0107fed9072",
            "protocol": "ids-multipart",
            "state": "CONFIRMED",
            "type": "CONSUMER"
        }
        ```
        * 내부 값들은 통신 시점에 따라 달라질 수 있습니다.
    * "contractAgreementId" 값을 복사해 둡니다.

12. File Transfer를 진행합니다.
    * [설정->Consumer](###Consumer)에서 설정한 주소 기반으로 생성한 REST API를 사용하여 설정 진행

    * URI = http://\<Consumer IP\>:9192/api/v1/management/transferprocess
    * HTTP METHOD = POST
    * Header 추가 목록
        * x-api-key = password
        * Content-Type = application/json
    * Body에 **"REST_API/transferprocess_post.json"** 내용을 입력
        ```json
        {
            "edctype": "dataspaceconnector:datarequest",
            "protocol": "ids-multipart",
            "assetId": "test-document",
            "contractId": "1:27c2c1a6-6895-474b-a7a9-0dbe52801aa6",
            "dataDestination": {
                "properties": {
                    "baseUrl": "http://\<Consumer IP\>:9191/api/receiveTransfer/test",
                    "type": "HttpData",
                    "authKey": "x-api-key",
                    "authCode": "password"
                }
            },
            "transferType": {
                "contentType": "application/octet-stream",
                "isFinite": true
            },
            "managedResources": false,
            "connectorAddress": "http://\<Provider IP\>:8282/api/v1/ids/data",
            "connectorId": "consumer"
        }
        ```
        * "contractId"의 값에 이전 Contract Negotiation 결과 검색에서 복사해두었던 "contractAgreementId" 값을 입력합니다.
    * 위와 같이 설정 한 뒤 통신 시 아래처럼 Response를 얻게 됩니다.
        ```json
        {
            "createdAt": 1675146113629,
            "id": "cf2c18a9-7b70-4487-b54a-a2fd37eadab6"
        }
        ```
        * "createdAt" 및 "id" 는 통신 시점에 따라 달라질 수 있습니다.
    * body 내 값에 잘못된 값을 입력 시 response를 수신하였더라도 전송 실패 할 수 있습니다.
=======
## 사용 방법

### 설치

1. git clone을 하여 프로젝트 복사

```
git clone https://gitlab.com/VCP-X/example/file-transfer-example.git
```



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!



## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/VCP-X/example/file-transfer-example.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/VCP-X/example/file-transfer-example/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
>>>>>>> origin/main
